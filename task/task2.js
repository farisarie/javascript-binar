/* 
Buat fungsi untuk menghitung luas dan keliling lingkaran
 Masukkan rumus keliling lingkaran & masukkan rumus luas lingkaran
*/

function menghitungLingkaran() {
  const pi = 3.14;
  let r = 7;

  let luas = pi * (r ** 2);
  let keliling = 2 * pi * r;
   

    console.log(`Berikut luas lingkaran: ${luas} dan keliling: ${keliling}`);
}

menghitungLingkaran();
