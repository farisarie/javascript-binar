// #1 Gunakan metode untuk membalikan isi array

let fruits = ["Apple", "Banana", "Papaya", "Grape", "Cherry", "Peach"];
let descending = fruits.reverse();
console.log(descending);
/* 
Output: 
Peach
Cherry
Grape
Papaya
Banana
Apple
*/

// #2 Gunakan metode untuk menyisipkan elemen baru dalam array

let month = [
  "January",
  "February",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

month.splice(2, 0, "March", "April", "May", "June");
console.log(month);

/* 
Output: 
 ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
*/

// #3 Mengambil beberapa kata terakhir dalam string
const message = "Sampaikan pada Sabrina belajar javascript sangat menyenangkan";

let substract = message.substring(23);
console.log(substract); 


/* 
Output: belajar Javascript sangat menyenangkan
*/

// #4 Buat agar awal kalimat jadi kapital

const modifiedString = substract.charAt(0).toUpperCase() + substract.slice(1);

console.log(modifiedString);

/* 
Output: Belajar Javascript sangat menyenangkan
*/

/* #5 Buat function untuk menghitung BMI (Body Mass Index)
- BMI = mass / height ** 2 = mass / (height * height) (mass in kg and height in meter)
- Steven weights 78 kg and is 1.69 m tall. Bill weights 92 kg and is 1.95 
m tall
*/

const calculate = (mass, height) => {
    let bmi = mass / (height**2);
    return bmi;
}

let john = calculate(95, 1.88);
console.log(john.toFixed(1));

let nash = calculate(85, 1.76);
console.log(nash.toFixed(1));

/* #6 Menghitung BMI dengan if else statement
- John weights 95 kg and is 1.88 m tall. Nash weights 85 kg and is 1.76 
m tall
*/
if (john > nash) {
    console.log(`John's BMI ${john.toFixed(1)} is higher than Nash's ${nash.toFixed(1)}`);
} else {
    console.log(`Nash's BMI ${nash.toFixed(1)} is higher than John's ${john.toFixed(1)}`);
}

/*
Output example:
John's BMI (28.3) is higher than Nash's (23.5)
*/

//  #7 Looping

let data = [10, 20, 30, 40, 50];
let total = 0;

/* You code here (you are allowed to reassigned the variable) 
Maybe you can write 3 lines or more
Use for, do while, while for, or forEach
*/
for(let i=0; i < data.length; i++) {
    total += data[i];
}
console.log(`Jumlah total = ${total}`);

/* 
Jumlah total = 150
*/
