// Module/helper --> Mix-ins
// Sub class

class Human {
  constructor(name, address, age) {
    this.name = name;
    this.address = address;
    this.age = age;
  }

  greet() {
    console.log(`Hi, my name is ${this.name}`);
  }

  work() {
    console.log(`${this.constructor.name}:`, "Working!");
  }
}

// Public Server Mode/Helper
const PublicServer = (Base) =>
  class extends Base {
    save() {
      console.log("SFX: Thank You!");
    }
  };

// Military Module/Helper
const Military = (Base) =>
  class extends Base {
    shoot() {
      console.log("DOR!");
    }
  };

class Doctor extends PublicServer(Human) {
  constructor(props) {
    super(props);
  }

  work() {
    super.work(); // From Human Class (Super class)
    super.save(); // From Public Server Class (Sub class)
    super.shoot(); // From Military Class (Sub class)
  }
}

class Police extends PublicServer(Military(Human)) {
  static workplace = "Police Station";

  constructor(props) {
    super(props);
    this.rank = props.rank;
  }

  work() {
    super.work(); // From Human Class (Super class)
    super.save(); // From Public Server Class (Sub class)
    super.shoot(); // From Military Class (Sub class)
  }
}

class Army extends PublicServer(Military(Human)) {
  static workplace = "Police Station";

  constructor(props) {
    super(props);
    this.rank = props.rank;
  }

  work() {
    super.work(); // From Human Class (Super class)
    super.save(); // From Public Server Class (Sub class)
    super.shoot(); // From Military Class (Sub class)
  }
}

class Writer extends Human {
  work() {
    console.log("Write books");
    super.work();
  }
}

// Instantiate Military Based Class
const Naruto = new Police({
  name: "Naruto",
  address: "Konoha",
  rank: "General",
});

const Sasuke = new Army({
  name: "Sasuke",
  address: "Konoha",
  rank: "General",
});

// Instantiate Doctor
const Sakura = new Doctor({
  name: "Sakura",
  address: "Konoha",
});

// Instantiate Writer
const Doraemon = new Writer({
  name: "Doraemon",
  address: "Konoha",
});

Sakura.save();
Naruto.shoot();
Doraemon.work();
Sasuke.shoot();
