class User {
  constructor(props) {
    // props is object, because it is better that way
    let { email, password } = props;
    this.email = email;
    this.encryptedPassword = this.#encrypt(password);
    // We won't save the plain password
  }

  // Private method
  #encrypt = (password) => {
    return `encrypted-version-of-${password}`;
  }; // hkhkhkhkhklllll09988888-klll-

  // Getter
  #decrypt = () => {
    return this.encryptedPassword.split(`encrypted-version-of-`)[1];
  }; // 123456

  authenticate(password) {
    return this.#decrypt() === password; // Return true or false
  }
} // 123456

let Bot = new User({
  email: "sabrina@mail.com",
  password: "123456",
});

const isAuthenticated = Bot.authenticate("12345");
console.log(isAuthenticated);
