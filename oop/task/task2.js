/**
 * Instruksi
 * 1. Buat ulang  Task#1, tetapi kali ini menggunakan kelas ES6 (sebut saja 'CarCl')
 * 2. Add getter called 'speedUS' yang mengembalikan kecepatan saat ini dalam mil/jam (bagi dengan 1.6)
 * 3. Add setter yang disebut 'speedUS' yang menyetel kecepatan saat ini dalam mil/jam (tetapi
 * lakukan terlebih dahulu konversi menjadi km/jam sebelum menyimpan nilainya, dengan mengalikan input dengan 1,6)
 * 4. Buat a new car and experiment with the 'accelerate' and 'brake' methods, and with the getter and setter.
 */

class CarCl {
  constructor(make, speed) {
   this.make = make;
   this.speed = speed;
  }

  accelerate() {
   this.speed += 10;
    console.log(`${this.make} is going at ${this.speed} km/h`);
  }

  brake() {
  this.speed -= 10;
    console.log(`${this.make} is going at ${this.speed} km/h`);
  }

  get speedUS() {
    return this.speed / 1.6;
  }

  set speedUS(speed) {
    this.speed = speed * 1.6;
  }
}

/**
 * Your code here (inisiasi with keyword new)
 */

const ford = new CarCl("Ford", 120);

console.log(ford.speedUS);
ford.accelerate();
ford.accelerate();
ford.brake();
ford.speedUS = 50;
console.log(ford);

/**
 * Output:
 * 75
 * Ford is going at 130 km/h
 * Ford is going at 140 km/h
 * Ford is going at 135 km/h
 * CarCl { make: 'Ford'; speed: 80}
 */
