class Human {
    // Static property
    static LivingOnAPrayer = true;
  
    // Add constructor method --> instance
    constructor(name, address, age, gender) {
      this.name = name;
      this.address = address;
      this.age = age;
      this.gender = gender;
    }
  
    // Add instance method signature
    introduce() {
      console.log(`Hi, my name is ${this.name}`);
    }
  
    // Visibility = public static
    static confess(hobby) {
      let hobbies = ["dancing", "coding", "singing"]
      return hobbies.includes(hobby.toLowerCase())
    }
  
    work() {
      console.log("Work!");
    }

    #doGossip = () => {
        console.log(`My address will become viral ${this.address}`)
    }

    talk() {
        console.log(this.#doGossip())
    }

    static #isHidingArea = true
  }

let mj = new Human("Sabrina", "Jakarta", "23", "Female");

console.log(mj.talk())

try {
    //Human.#isHidingArea;
    //mj.#doGossip()
} catch (err) {
    console.error(err)
}
