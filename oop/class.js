class Human {
  // Static property
  static LivingOnAPrayer = true;

  // Add constructor method --> instance
  constructor(name, address, age, gender) {
    this.name = name;
    this.address = address;
    this.age = age;
    this.gender = gender;
  }

  // Add instance method signature
  introduce() {
    console.log(`Hi, my name is ${this.name}`);
  }

  confess() {
    console.log(`I am ${this.age} years`);
  }

  work() {
    console.log("Work!");
  }
}

// call static property
console.log(Human.LivingOnAPrayer);

// Add prototype / instance method
Human.prototype.greet = function (name) {
  console.log(`Hi, ${name}, I'm ${this.name} and my address ${this.address}`);
};

// Add static method
Human.destroy = function (thing) {
  console.log(`Human is destroying ${thing}`);
};

// Instantiation Human class, create new object
let mj = new Human("Sabrina", "Jakarta", "23", "Female");
console.log(mj);

// Checking instance of class
console.log(mj instanceof Human);
console.log(mj.introduce());
console.log(mj.confess());
console.log(mj.greet("Sabrina"));
console.log(Human.destroy("Phone"));
