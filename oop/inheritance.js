// Super class / parent class
class Human {
  // Static property
  static LivingOnAPrayer = true;

  // Add constructor method --> instance
  constructor(name, address, age, gender) {
    this.name = name;
    this.address = address;
    this.age = age;
    this.gender = gender;
  }

  // Add instance method signature
  introduce() {
    console.log(`Hi, my name is ${this.name}`);
  }

  confess() {
    console.log(`I am ${this.age} years`);
  }

  work() {
    console.log("Work!");
  }

  _call() {
    console.log(`My names is ${this.name}`)
  }
}

// Inheritance --> pewarisan
class Badut extends Human {
  constructor(name, address, skill) {
    super(name, address);
    this.skill = skill;
  }

  //Overriding;
  introduce() {
    super.introduce(); // Call the super class introduce method
    console.log(`I can ${this.skill}`);
  }

  /* if (this.skill > 3) {

  } 
  else if (this.skill <= 3) {

  }
  else {

  }
  
  */

  // Overloading
  introduce(withDetail) {
    super.introduce();
    Array.isArray(withDetail)
      ? console.log("Wrong input")
      : console.log(`I can ${this.skill}`)
  }

  magic() {
    console.log(
      "magic some",
      this.skill[Math.floor(Math.random() * this.skill.length)]
    );
  }

  // Protect
  doCall () {
    super._call()
  }
}

// create new object
let John = new Human("John Doe", "London", "26", "Male");

// create new object of badut class
let Bill = new Badut("Bill Badut", " Jakarta", [
  "Make up",
  " Fly",
  " Dance",
  " Juggling",
]);
// Bill.magic();
// Bill.work();
Bill.introduce("Fly");

// Overriding


// Protect
console.log(Bill._call())